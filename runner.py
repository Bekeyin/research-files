def run_function(function_name, parameters):
    import importlib

    import requests

    module = importlib.import_module("functions")

    print("Params", function_name, parameters)
    func = None
    try:
        func = getattr(module, function_name)
    except KeyError as ke:
        return {"success": False, "error": f"{ke}"}
    try:
        print("function in runner", func, parameters)
        response = func(**parameters)
    except Exception as e:
        print("Error occured while executing function: ", e.__class__.__name__, e)
        return {"success": False, "error": f"{e.__class__.__name__}: {e}"}
    else:
        print("Dammm working")
        if response:
            if isinstance(response, dict):
                return response
            else:
                return {"success": True, "response": f"{response}"}
        return (
            {"success": True, "message": response}
            if response is not None
            else {"success": True, "message": "Successfully runned"}
        )
