import json
from ast import literal_eval

from openai import Client

from runner import run_function
from setup import OPEN_AI_API_KEY

TOOLS = [
    {
        "type": "function",
        "function": {
            "name": "get_products",
            "description": "Get all the products",
        },
    }
]


class OpenAiGpt3Turbo:
    __client = Client(api_key=OPEN_AI_API_KEY)
    __model = "gpt-3.5-turbo"

    def __init__(self, system_message, tools=TOOLS) -> None:
        self.system_message = system_message
        self.tools = tools
        self.messages = [{"role": "system", "content": system_message}]

    def add_user_message(self, user_message):
        self.messages.append({"role": "user", "content": user_message})

    def check_is_function_calling(self, message):
        if message["finish_reason"] == "tool_calls":
            return True
        else:
            return False

    def run_conversation(self):
        response = self.__client.chat.completions.create(
            model=self.__model,
            messages=self.messages,
            tools=self.tools,
            tool_choice="auto",
        )
        message = response.choices[0].message
        tool_calls = response.choices[0].message.tool_calls
        if tool_calls:
            self.messages.append(message.dict())
            for tool_call in tool_calls:
                function_name = tool_call.function.name
                arguments = literal_eval(tool_call.function.arguments)
                response = run_function(function_name, arguments)
                self.messages.append(
                    {
                        "tool_call_id": tool_call.id,
                        "role": "tool",
                        "name": function_name,
                        "content": json.dumps(response),
                    }
                )
            return self.run_conversation()
        else:
            print("\n\n Assistant: \n", message.content)
            return message.content

    def run(self):
        return self.run_conversation()
