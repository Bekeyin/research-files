from function_callings.gpt3 import OpenAiGpt3Turbo


def main():
    system_message = "You are a helpful assistant."
    gpt3 = OpenAiGpt3Turbo(system_message)
    gpt3.add_user_message("Give me all the product titles in list format.")
    gpt3.run()


if __name__ == "__main__":
    main()
