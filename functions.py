import json
import time

PRODUCTS = [
    {
        "id": "gid://shopify/Product/8392400077108",
        "title": "Breezy Comfort: Essential Short Sleeves for Effortless Style",
        "productType": "",
        "totalVariants": 1,
        "vendor": "creators-test",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "1200.0", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "1200.0", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427448566068",
        "title": "VANS |AUTHENTIC | LO PRO | BURGANDY/WHITE",
        "productType": "SHOES",
        "totalVariants": 3,
        "vendor": "VANS",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "29.0", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "29.0", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427448729908",
        "title": "VANS | AUTHENTIC | (MULTI EYELETS) | GRADIENT/CRIMSON",
        "productType": "SHOES",
        "totalVariants": 5,
        "vendor": "VANS",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "99.95", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "99.95", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427448893748",
        "title": "VANS | AUTHENTIC (BUTTERFLY) TRUE | WHITE / BLACK",
        "productType": "SHOES",
        "totalVariants": 6,
        "vendor": "VANS",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "109.95", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "109.95", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427449090356",
        "title": "VANS | \nERA 59 MOROCCAN | GEO/DRESS BLUES",
        "productType": "SHOES",
        "totalVariants": 4,
        "vendor": "VANS",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "119.95", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "119.95", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427449221428",
        "title": "VANS | SK8-HI DECON (CUTOUT)| LEAVES/WHITE",
        "productType": "SHOES",
        "totalVariants": 7,
        "vendor": "VANS",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "179.95", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "179.95", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427449418036",
        "title": "PALLADIUM | PALLATECH HI TX | CHEVRON",
        "productType": "SHOES",
        "totalVariants": 3,
        "vendor": "PALLADIUM",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "159.95", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "159.95", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427449549108",
        "title": "ASICS TIGER | GEL-LYTE V '30 YEARS OF GEL' PACK",
        "productType": "SHOES",
        "totalVariants": 3,
        "vendor": "ASICS TIGER",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "220.0", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "220.0", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427449680180",
        "title": "PUMA | SUEDE CLASSIC REGAL",
        "productType": "SHOES",
        "totalVariants": 4,
        "vendor": "PUMA",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "110.0", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "110.0", "currencyCode": "NPR"},
        },
    },
    {
        "id": "gid://shopify/Product/8427449811252",
        "title": "SUPRA | MENS VAIDER",
        "productType": "SHOES",
        "totalVariants": 4,
        "vendor": "SUPRA",
        "priceRangeV2": {
            "maxVariantPrice": {"amount": "169.95", "currencyCode": "NPR"},
            "minVariantPrice": {"amount": "169.95", "currencyCode": "NPR"},
        },
    },
]


def get_products(**kwargs):
    print("kwargs", kwargs)
    time.sleep(5)
    return PRODUCTS
